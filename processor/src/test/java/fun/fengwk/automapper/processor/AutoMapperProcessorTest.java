package fun.fengwk.automapper.processor;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;
import org.junit.Test;

import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import java.io.IOException;

import static com.google.testing.compile.CompilationSubject.assertThat;

/**
 * @author fengwk
 */
public class AutoMapperProcessorTest {

    @Test
    public void testDemo() {
        Compilation compilation = Compiler
                .javac()
                .withProcessors(new AutoMapperProcessor())
                .compile(
                        JavaFileObjects.forResource("fun/fengwk/automapper/processor/demo/DemoDO.java"),
                        JavaFileObjects.forResource("fun/fengwk/automapper/processor/demo/DemoMapper.java")
                );
        assertThat(compilation).succeeded();
        // 确认InMemoryJavaObjects的内容
        compilation.generatedFiles().forEach(file -> {
            // xml文件的种类是Kind.OTHER
            if (file.getKind().equals(JavaFileObject.Kind.OTHER)) {
                CharSequence charContent = null;
                try {
                    charContent = file.getCharContent(false);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                // 打印automapper生成的xml文件
                System.out.println(charContent.toString());
            }
        });
        assertThat(compilation).generatedFile(StandardLocation.CLASS_OUTPUT, "fun/fengwk/automapper/processor/demo/DemoMapper.xml");
    }

    @Test
    public void testExample() {
        Compilation compilation = Compiler
                .javac()
                .withProcessors(new AutoMapperProcessor())
                .compile(
                        JavaFileObjects.forResource("fun/fengwk/automapper/example/mapper/BaseMapper.java"),
                        JavaFileObjects.forResource("fun/fengwk/automapper/example/mapper/EmptyMapper.java"),
                        JavaFileObjects.forResource("fun/fengwk/automapper/example/mapper/ExampleMapper.java"),
                        JavaFileObjects.forResource("fun/fengwk/automapper/example/mapper/SimpleExampleMapper.java"),
                        JavaFileObjects.forResource("fun/fengwk/automapper/example/model/BaseDO.java"),
                        JavaFileObjects.forResource("fun/fengwk/automapper/example/model/EmptyDO.java"),
                        JavaFileObjects.forResource("fun/fengwk/automapper/example/model/ExampleDO.java"),
                        JavaFileObjects.forResource("fun/fengwk/automapper/example/model/SimpleExampleDO.java")
                );
        assertThat(compilation).succeeded();
        assertThat(compilation).generatedFile(StandardLocation.CLASS_OUTPUT, "fun/fengwk/automapper/example/mapper/EmptyMapper.xml");
        assertThat(compilation).generatedFile(StandardLocation.CLASS_OUTPUT, "fun/fengwk/automapper/example/mapper/ExampleMapper.xml");
        assertThat(compilation).generatedFile(StandardLocation.CLASS_OUTPUT, "fun/fengwk/automapper/example/mapper/SimpleExampleMapper.xml");
    }

}
