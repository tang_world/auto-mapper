package fun.fengwk.automapper.processor.translator;

/**
 * Bean属性对象，负责映射java字段和数据库字段
 */
public class BeanField implements SelectiveNameEntry {

    /**
     * java程序中的名称。
     */
    private String name;

    /**
     * 数据库中的字段名称。
     */
    private String fieldName;

    /**
     * 是否使用useGeneratedKeys。
     */
    private boolean useGeneratedKeys;

    /**
     * 当前字段是否是可选的。
     */
    private final boolean isSelective;

    public BeanField(String name, String fieldName, boolean useGeneratedKeys, boolean isSelective) {
        this.name = name;
        this.fieldName = fieldName;
        this.useGeneratedKeys = useGeneratedKeys;
        this.isSelective = isSelective;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    public boolean isUseGeneratedKeys() {
        return useGeneratedKeys;
    }

    @Override
    public boolean isSelective() {
        return isSelective;
    }
}
