package fun.fengwk.automapper.processor;

import fun.fengwk.automapper.annotation.AutoMapper;
import fun.fengwk.automapper.annotation.DBType;
import fun.fengwk.automapper.annotation.NamingStyle;
import fun.fengwk.automapper.processor.mapper.GlobalConfig;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ExecutableElement;
import java.util.Map;

/**
 *
 * @see AutoMapper
 * @author fengwk
 */
public class AutoMapperInfo {

    private final DBType dbType;
    private final String mapperSuffix;
    private final NamingStyle tableNamingStyle;
    private final NamingStyle fieldNamingStyle;
    private final String tableName;
    private final String tableNamePrefix;

    private AutoMapperInfo(DBType dbType, String mapperSuffix, NamingStyle tableNamingStyle,
                          NamingStyle fieldNamingStyle, String tableName, String tableNamePrefix) {
        this.dbType = dbType;
        this.mapperSuffix = mapperSuffix;
        this.tableNamingStyle = tableNamingStyle;
        this.fieldNamingStyle = fieldNamingStyle;
        this.tableName = tableName;
        this.tableNamePrefix = tableNamePrefix;
    }

    /**
     * 解析加载AutoMapper的配置信息
     * @param autoMapper 注解对象
     * @param autoMapperMirror 注解的镜像，和原注解对象不保证是同一个对象
     * @param globalConfig
     * @return
     */
    public static AutoMapperInfo parse(AutoMapper autoMapper, AnnotationMirror autoMapperMirror, GlobalConfig globalConfig) {
        // 规则：用户注解明确设置 > 全局配置 > 默认设置

        DBType dbType = autoMapper.dbType();
        String mapperSuffix = autoMapper.mapperSuffix();
        NamingStyle tableNamingStyle = autoMapper.tableNamingStyle();
        NamingStyle fieldNamingStyle = autoMapper.fieldNamingStyle();
        String tableName = autoMapper.tableName();
        String tableNamePrefix = autoMapper.tableNamePrefix();

        DBType globalDbType = globalConfig.getDBType();
        String globalMapperSuffix = globalConfig.getMapperSuffix();
        NamingStyle globalTableNamingStyle = globalConfig.getTableNamingStyle();
        NamingStyle globalFieldNamingStyle = globalConfig.getFieldNamingStyle();
        String globalTableNamePrefix = globalConfig.getTableNamePrefix();

        if (globalDbType != null && isNotExplicit(autoMapperMirror, "dbType")) {
            dbType = globalDbType;
        }
        if (globalMapperSuffix != null && isNotExplicit(autoMapperMirror, "mapperSuffix")) {
            mapperSuffix = globalMapperSuffix;
        }
        if (globalTableNamingStyle != null && isNotExplicit(autoMapperMirror, "tableNamingStyle")) {
            tableNamingStyle = globalTableNamingStyle;
        }
        if (globalFieldNamingStyle != null && isNotExplicit(autoMapperMirror, "fieldNamingStyle")) {
            fieldNamingStyle = globalFieldNamingStyle;
        }
        if (globalTableNamePrefix != null && isNotExplicit(autoMapperMirror, "tableNamePrefix")) {
            tableNamePrefix = globalTableNamePrefix;
        }

        return new AutoMapperInfo(dbType, mapperSuffix, tableNamingStyle, fieldNamingStyle, tableName, tableNamePrefix);
    }

    // 检查注解方法是否被用户明确设置了
    private static boolean isNotExplicit(AnnotationMirror autoMapperMirror, String methodName) {
        Map<? extends ExecutableElement, ? extends AnnotationValue> elementValues = autoMapperMirror.getElementValues();
        for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> e : elementValues.entrySet()) {
            if (methodName.equals(e.getKey().getSimpleName().toString())) {
                return false;
            }
        }
        return true;
    }

    public DBType getDbType() {
        return dbType;
    }

    public String getMapperSuffix() {
        return mapperSuffix;
    }

    public NamingStyle getTableNamingStyle() {
        return tableNamingStyle;
    }

    public NamingStyle getFieldNamingStyle() {
        return fieldNamingStyle;
    }

    public String getTableName() {
        return tableName;
    }

    public String getTableNamePrefix() {
        return tableNamePrefix;
    }
}
