package fun.fengwk.automapper.processor.mapper;

import fun.fengwk.automapper.annotation.ExcludeField;
import fun.fengwk.automapper.annotation.FieldName;
import fun.fengwk.automapper.annotation.IncludeField;
import fun.fengwk.automapper.annotation.Selective;
import fun.fengwk.automapper.annotation.UseGeneratedKeys;
import fun.fengwk.automapper.processor.naming.NamingConverter;
import fun.fengwk.automapper.processor.translator.BeanField;
import fun.fengwk.automapper.processor.translator.MethodInfo;
import fun.fengwk.automapper.processor.translator.Param;
import fun.fengwk.automapper.processor.translator.Return;
import fun.fengwk.automapper.processor.util.StringUtils;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author fengwk
 */
public class MapperMethodParser {

    private static final Pattern PATTERN_GETTER = Pattern.compile("^get(.+)$");
    private static final Pattern PATTERN_SETTER = Pattern.compile("^set(.+)$");

    // 代理接口方法的条件： 不是native、static、default修饰的非object类的方法
    private static final List<Predicate<ExecutableElement>> METHOD_FILTERS = Arrays.asList(
            methodElement -> !methodElement.getModifiers().contains(Modifier.NATIVE),
            methodElement -> !methodElement.getModifiers().contains(Modifier.STATIC),
            methodElement -> !methodElement.isDefault(),
            methodElement -> !isObjectMethod(methodElement)
    );

    private static boolean isObjectMethod(ExecutableElement methodElement) {
        TypeElement enclosingElement = (TypeElement) methodElement.getEnclosingElement();
        return Object.class.getName().equals(enclosingElement.getQualifiedName().toString());
    }

    private final Types types;
    private final Elements elements;

    public MapperMethodParser(Types types, Elements elements) {
        this.types = types;
        this.elements = elements;
    }

    public List<MethodInfo> parse(TypeElement mapperElement, NamingConverter fieldNamingConverter) {
        return collectMethodElements(mapperElement).stream()
                .filter(this::filterMethodElement)
                .map(methodElement -> convert(methodElement, mapperElement, fieldNamingConverter))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private Set<ExecutableElement> collectMethodElements(TypeElement mapperElement) {
        Set<ExecutableElement> methodElementCollector = new LinkedHashSet<>();
        doCollectMethodElements(mapperElement, methodElementCollector);
        return methodElementCollector;
    }

    /**
     * 递归获取类接口以及超类接口的方法，收集这些方法到收集器中
     *
     * @param mapperElement          当前递归的类对象，可能有超类接口
     * @param methodElementCollector 收集器
     */
    private void doCollectMethodElements(TypeElement mapperElement, Set<ExecutableElement> methodElementCollector) {
        List<? extends TypeMirror> interfaces = mapperElement.getInterfaces();
        for (TypeMirror typeMirror : interfaces) {
            Element superMapperElement = types.asElement(typeMirror);
            doCollectMethodElements((TypeElement) superMapperElement, methodElementCollector);
        }

        List<? extends Element> memberElements = elements.getAllMembers(mapperElement);
        for (Element memberElement : memberElements) {
            if (memberElement.getKind() == ElementKind.METHOD) {
                ExecutableElement methodElement = (ExecutableElement) memberElement;
                methodElementCollector.add(methodElement);
            }
        }
    }

    /**
     * 如果包含static、default、native关键字或者是object的方法，那么返回false
     * 否则true
     *
     * @param executableElement
     * @return
     */
    private boolean filterMethodElement(ExecutableElement executableElement) {
        for (Predicate<ExecutableElement> methodFilter : METHOD_FILTERS) {
            if (!methodFilter.test(executableElement)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 将接口带实现的方法，即methodElement，转换为MethodInfo
     * 如果使用 {@link Insert} 等任何sql映射注解，返回null
     *
     * @param methodElement
     * @param mapperElement
     * @param fieldNamingConverter
     * @return if method is implements by annatation ,return null,otherwise return converted MethedInfo.
     */
    private MethodInfo convert(ExecutableElement methodElement, TypeElement mapperElement, NamingConverter fieldNamingConverter) {
        if (methodElement.getAnnotation(Insert.class) != null
                || methodElement.getAnnotation(Delete.class) != null
                || methodElement.getAnnotation(Update.class) != null
                || methodElement.getAnnotation(Select.class) != null) {
            return null;
        }

        String methodName = methodElement.getSimpleName().toString();
        Set<String> includeFieldNames = getIncludeFieldNames(methodElement);
        Set<String> excludeFieldNames = getExcludeFieldNames(methodElement);
//        List<Anno> annos = parseAnnotations(methodElement);
        List<Param> params = new ArrayList<>();
        List<? extends VariableElement> methodParameters = methodElement.getParameters();
        if (methodParameters != null) {
            for (VariableElement methodParameter : methodParameters) {
                TypeDescriptor desc = new TypeDescriptor();
                if (desc.parse(mapperElement, methodParameter.asType(), fieldNamingConverter)) {
                    String javaName = methodParameter.getSimpleName().toString();
                    org.apache.ibatis.annotations.Param paramAnnotation = methodParameter.getAnnotation(org.apache.ibatis.annotations.Param.class);
                    if (paramAnnotation != null) {
                        javaName = paramAnnotation.value();
                    }

                    FieldName fieldNameAnnotation = methodParameter.getAnnotation(FieldName.class);
                    String fieldName = fieldNameAnnotation != null ? fieldNameAnnotation.value()
                            : fieldNamingConverter.convert(StringUtils.upperCamelToLowerCamel(javaName));

                    params.add(new Param(desc.type, javaName, fieldName, desc.isIterable, desc.isJavaBean,
                            getAndFilterBeanFields(desc, includeFieldNames, excludeFieldNames),
                            methodParameter.getAnnotation(Selective.class) != null));
                }
            }
        }

        TypeDescriptor desc = new TypeDescriptor();
        if (desc.parse(mapperElement, methodElement.getReturnType(), fieldNamingConverter)) {
            Return ret = new Return(desc.type, desc.isJavaBean, desc.beanFields);
            return new MethodInfo(methodName, params, ret);
        }

        return null;// 解析失败
    }

    private Set<String> getIncludeFieldNames(ExecutableElement methodElement) {
        IncludeField includeField = methodElement.getAnnotation(IncludeField.class);
        IncludeField.List includeFieldList = methodElement.getAnnotation(IncludeField.List.class);
        if (includeField == null && includeFieldList == null) {
            return null;
        }

        Set<String> includeFieldNames = new HashSet<>();
        if (includeField != null && includeField.value() != null) {
            includeFieldNames.add(includeField.value());
        }
        if (includeFieldList != null) {
            IncludeField[] includeFieldInListArray = includeFieldList.value();
            if (includeFieldInListArray != null) {
                for (IncludeField includeFieldInList : includeFieldInListArray) {
                    if (includeFieldInList != null && includeFieldInList.value() != null) {
                        includeFieldNames.add(includeFieldInList.value());
                    }
                }
            }
        }
        return includeFieldNames;
    }

    private Set<String> getExcludeFieldNames(ExecutableElement methodElement) {
        ExcludeField excludeField = methodElement.getAnnotation(ExcludeField.class);
        ExcludeField.List excludeFieldList = methodElement.getAnnotation(ExcludeField.List.class);
        if (excludeField == null && excludeFieldList == null) {
            return null;
        }

        Set<String> excludeFieldNames = new HashSet<>();
        if (excludeField != null && excludeField.value() != null) {
            excludeFieldNames.add(excludeField.value());
        }
        if (excludeFieldList != null) {
            ExcludeField[] excludeFieldInListArray = excludeFieldList.value();
            if (excludeFieldInListArray != null) {
                for (ExcludeField excludeFieldInList : excludeFieldInListArray) {
                    if (excludeFieldInList != null && excludeFieldInList.value() != null) {
                        excludeFieldNames.add(excludeFieldInList.value());
                    }
                }
            }
        }
        return excludeFieldNames;
    }

//    private List<Anno> parseAnnotations(ExecutableElement methodElement) {
//        List<Anno> annos = new ArrayList<>();
//        List<? extends AnnotationMirror> annotationMirrors = methodElement.getAnnotationMirrors();
//        if (annotationMirrors != null) {
//            for (AnnotationMirror annotationMirror : annotationMirrors) {
//                annos.add(new Anno(annotationMirror.getAnnotationType().toString()));
//            }
//        }
//        return annos;
//    }

    private List<BeanField> getAndFilterBeanFields(TypeDescriptor desc, Set<String> includeFieldNames, Set<String> excludeFieldNames) {
        if (!desc.isJavaBean) {
            return null;
        }

        List<BeanField> result = new ArrayList<>();
        for (BeanField bf : desc.beanFields) {
            if (includeFieldNames != null) {
                if (!includeFieldNames.contains(bf.getName())) {
                    continue;
                }
            }
            if (excludeFieldNames != null) {
                if (excludeFieldNames.contains(bf.getName())) {
                    continue;
                }
            }
            result.add(bf);
        }
        return result;
    }

    /**
     * 类型描述器
     */
    class TypeDescriptor {

        String type;
        boolean isIterable;
        boolean isJavaBean;
        List<BeanField> beanFields;

        // 默认的深度为0
        public boolean parse(TypeElement mapperElement, TypeMirror typeMirror, NamingConverter fieldNamingConverter) {
            return parse(mapperElement,typeMirror,fieldNamingConverter,0);
        }
        /**
         * 解析类型名称和属性，成功返回true
         * 如果解析失败，返回false，此时该类的属性不应该被使用
         *
         * @param mapperElement        类元素，最全的元数据对象
         * @param typeMirror           类镜像， 从上一个参数值中取出的某个类型镜像
         * @param fieldNamingConverter 属性转换风格
         * @param depth                深度，最大深度为1，深度从0开始
         * @return
         */
        public boolean parse(TypeElement mapperElement, TypeMirror typeMirror, NamingConverter fieldNamingConverter, int depth) {
            if (depth > 1) {
                // 最多解析到第二层，例如参数为List<T>，最多解析到T类型
                return false;
            }
            switch (typeMirror.getKind()) {
                case BOOLEAN:
                case BYTE:
                case SHORT:
                case INT:
                case LONG:
                case CHAR:
                case FLOAT:
                case DOUBLE:
                case VOID:
                    type = typeMirror.toString();
                    return true;
                case ARRAY:
                    ArrayType arrayType = (ArrayType) typeMirror;
                    TypeMirror componentTypeMirror = arrayType.getComponentType();
                    return parse(mapperElement, componentTypeMirror, fieldNamingConverter, depth + 1);
                case DECLARED://class or interface
                    if (isIterable(typeMirror)) {
                        isIterable = true;
                        DeclaredType declaredType = (DeclaredType) typeMirror;
                        List<? extends TypeMirror> typeArguments = declaredType.getTypeArguments();
                        if (!typeArguments.isEmpty()) {
                            return parse(mapperElement, typeArguments.get(0), fieldNamingConverter, depth + 1);
                        }
                        return false;
                    } else if (isJavaDeclared(typeMirror)) {//java 基础包下的类
                        TypeElement typeElement = ((TypeElement) types.asElement(typeMirror));
                        type = typeElement.getQualifiedName().toString();
                        return true;
                    } else {// 用户自定义类，并且没有实现Iterable接口
                        TypeElement typeElement = ((TypeElement) types.asElement(typeMirror));
                        type = typeElement.getQualifiedName().toString();
                        isJavaBean = true;
                        parseJavaBean(typeElement, fieldNamingConverter);
                        return true;
                    }
                case TYPEVAR: // 泛型变量类型
                    Set<TypeMirror> supertypes = collectSupertypes(mapperElement.asType());

                    TypeVariable typeVariable = (TypeVariable) typeMirror;
                    TypeMirror variableEnclosingType = typeVariable.asElement().getEnclosingElement().asType();

                    for (TypeMirror sup : supertypes) {
                        if (types.isSameType(types.asElement(sup).asType(), variableEnclosingType)) {
                            List<? extends TypeMirror> supTypeArguments = ((DeclaredType) sup).getTypeArguments();
                            List<? extends TypeMirror> typeArguments = ((DeclaredType) variableEnclosingType).getTypeArguments();
                            for (int i = 0; i < typeArguments.size(); i++) {
                                if (types.isSameType(typeArguments.get(i), typeVariable)) {
                                    // 如果当前也是类型和泛型一致，那么就去解析真正的泛型类型
                                    TypeMirror type = supTypeArguments.get(i);
                                    return parse(mapperElement, type, fieldNamingConverter, depth);
                                }
                            }
                        }
                    }
                default:
                    return false;
            }
        }

        // 收集所有父类型
        private Set<TypeMirror> collectSupertypes(TypeMirror typeMirror) {
            Set<TypeMirror> supertypes = new HashSet<>();
            doCollectSupertypes(typeMirror, supertypes);
            return supertypes;
        }

        private void doCollectSupertypes(TypeMirror typeMirror, Set<TypeMirror> supertypes) {
            List<? extends TypeMirror> typeMirrors = types.directSupertypes(typeMirror);
            for (TypeMirror sup : typeMirrors) {
                if (!supertypes.contains(sup)) {
                    supertypes.add(sup);
                    doCollectSupertypes(sup, supertypes);
                }
            }
        }

        // 判断typeMirror是否为Iterable
        private boolean isIterable(TypeMirror typeMirror) {
            TypeElement typeElement = (TypeElement) types.asElement(typeMirror);
            if (typeElement == null) {
                return false;
            }

            if (typeElement.getQualifiedName().toString().equals(Iterable.class.getName())) {
                return true;
            }

            TypeMirror superclass = typeElement.getSuperclass();
            if (superclass != null && isIterable(superclass)) {
                return true;
            }

            List<? extends TypeMirror> interfaces = typeElement.getInterfaces();
            if (interfaces != null) {
                for (TypeMirror inter : interfaces) {
                    if (isIterable(inter)) {
                        return true;
                    }
                }
            }

            return false;
        }

        private boolean isJavaDeclared(TypeMirror typeMirror) {
            TypeElement typeElement = (TypeElement) types.asElement(typeMirror);
            String name = typeElement.getQualifiedName().toString();
            return name.startsWith("java.applet")
                    || name.startsWith("java.awt")
                    || name.startsWith("java.beans")
                    || name.startsWith("java.io")
                    || name.startsWith("java.lang")
                    || name.startsWith("java.math")
                    || name.startsWith("java.net")
                    || name.startsWith("java.nio")
                    || name.startsWith("java.rmi")
                    || name.startsWith("java.security")
                    || name.startsWith("java.sql")
                    || name.startsWith("java.text")
                    || name.startsWith("java.time")
                    || name.startsWith("java.util")
//                    || name.startsWith("com.oracle")
//                    || name.startsWith("com.sun")
//                    || name.startsWith("javax.")
//                    || name.startsWith("org.ietf.jgss")
//                    || name.startsWith("org.jcp.xml.dsig.internal")
//                    || name.startsWith("org.omg")
//                    || name.startsWith("org.w3c.dom")
//                    || name.startsWith("org.xml.sax")
//                    || name.startsWith("sun.")
                    ;
        }

        // 解析JavaBean, 填充属性值beanFields
        private void parseJavaBean(TypeElement typeElement0, NamingConverter fieldNamingConverter) {
            // 获取当前类在最后，Object类在最前面的顺序排序后的list
            List<TypeElement> allTypeElements = collectSupertypes(typeElement0.asType()).stream()
                    .map(types::asElement)
                    .map(TypeElement.class::cast)
                    .collect(Collectors.toList());
            Collections.reverse(allTypeElements);
            allTypeElements.add(typeElement0);
            // 从父类开始解析继承属性值，在子类的时候判断是否已经有了，通过map去重
            Map<String, BeanField> beanFieldMap = new LinkedHashMap<>();
            for (TypeElement typeElement : allTypeElements) {
                List<? extends Element> allMembers = elements.getAllMembers(typeElement);
                for (Element element : allMembers) {
                    if (element.getKind() == ElementKind.FIELD) {
                        VariableElement fieldElement = (VariableElement) element;
                        if (!fieldElement.getModifiers().contains(Modifier.STATIC)) {
                            String javaName = fieldElement.getSimpleName().toString();
                            if (!beanFieldMap.containsKey(javaName)) {
                                FieldName fieldNameAnnotation = fieldElement.getAnnotation(FieldName.class);
                                String fieldName = fieldNameAnnotation != null ? fieldNameAnnotation.value()
                                        : fieldNamingConverter.convert(StringUtils.upperCamelToLowerCamel(javaName));
                                boolean useGeneratedKeys = fieldElement.getAnnotation(UseGeneratedKeys.class) != null;
                                beanFieldMap.put(javaName, new BeanField(javaName, fieldName, useGeneratedKeys,
                                        fieldElement.getAnnotation(Selective.class) != null));
                            }
                        }
                    }
                }
            }

            this.beanFields = new ArrayList<>(beanFieldMap.values());

            // 与lombok有执行顺序冲突，暂无法使用getter、setter方法校验bean字段正确性
            // 要解决这个问题，需要研究一下lombok的执行轮的数值，只要Auto Mapper的执行顺序在其后，就可以正常工作。
//            Set<String> getterMatchers = new HashSet<>();
//            Set<String> setterMatchers = new HashSet<>();
//            for (TypeElement typeElement : allTypeElements) {
//                List<? extends Element> allMembers = elements.getAllMembers(typeElement);
//                for (Element element : allMembers) {
//                    if (element.getKind() == ElementKind.METHOD) {
//                        ExecutableElement methodElement = (ExecutableElement) element;
//                        String methodName = methodElement.getSimpleName().toString();
//                        Matcher getterMatcher = PATTERN_GETTER.matcher(methodName);
//                        if (getterMatcher.find()) {
//                            getterMatchers.add(Introspector.decapitalize(getterMatcher.group(1)));
//                        } else {
//                            Matcher setterMatcher = PATTERN_SETTER.matcher(methodName);
//                            if (setterMatcher.find()) {
//                                setterMatchers.add(Introspector.decapitalize(setterMatcher.group(1)));
//                            }
//                        }
//                    }
//                }
//            }
//
//            List<BeanField> beanFields = new ArrayList<>();
//            for (Map.Entry<String, BeanField> entry :beanFieldMap.entrySet()) {
//                if (getterMatchers.contains(entry.getKey()) && setterMatchers.contains(entry.getKey())) {
//                    beanFields.add(entry.getValue());
//                }
//            }
//
//            this.beanFields = beanFields;
        }

    }

}
